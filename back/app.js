var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const session = require('express-session');
const cors = require('cors');
const errorHandler = require('errorhandler');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var productsRouter = require('./routes/products');
var feedbacksRouter = require('./routes/feedBack');
var serviceofferRouter = require('./routes/service_offer');
var shippingAddressRouter = require('./routes/shipping_address');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


/**Routes */
/**index */
app.use('/', indexRouter);
/**users */
app.use('/users', usersRouter);
/**products */
app.use('/product',productsRouter)
/**feedback */
app.use('/feedback',feedbacksRouter)
/**service offer */
app.use('/seriveOffer',serviceofferRouter)
/**shipping address */
app.use('/shippingAddress',shippingAddressRouter)
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({"message":err.message});
});

module.exports = app;
