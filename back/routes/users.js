var express = require('express');
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var router = express.Router();
// var db=require('../models/index');
var db =require('../models/index') 
var utils =require('../utils/secret') 
var VerifyToken=require('../utils/VerifyToken')

const User = db.sequelize.models.user;
const audit_logs = db.sequelize.models.audit_logs;

/**get all  user */
router.get('/', VerifyToken, function(req, res, next) {
  User.findAll().then( (result) => res.json(result)).catch((err)=>{
    res.json({"message":err.stack})
})
});
/**Create new user */
router.post('/', function(req, res, next) {
     console.log(req.body)
    //  if(req.body.names==''||req.body.names){
    //    res.json({"message":"Name are required"})
    //    return;
    //  }
    //  if(req.body.email==''||req.body.email){
    //   res.json({"message":"Email are required"})
    //   return;
    // }
    // if(req.body.password==''||req.body.password){
    //   res.json({"message":"Password are required"})
    //   return;
    // }
    let hashpassword=bcrypt.hashSync(req.body.password, 10);
   
  
     User.findOrCreate({where:{email:req.body.email},defaults:{
      names: req.body.names,
      email: req.body.email,
      password:hashpassword,
    }}).then( (result) =>  {
      if(result[1]){
        res.json(result[0])
      }else{
        res.json({"message":`User ${req.body.names} Exist`})
      }
    } ).catch((err)=>{
      res.json({"message":err.stack})
  })

});
/**update  user */
router.put('/:id', VerifyToken, function(req, res, next) {
  if(req.params.id==""){
    res.status(400).json({"message":"Url params are required"})
  }
  
  User.update({
   names: req.body.names,
   email: req.body.email
 },{where:{id:req.params.id }}).then( (result) => res.json(result) ).catch((err)=>{
  res.status(500).json({"message":err.stack})
})

});

/**delete  user */
router.delete('/:id', VerifyToken, function(req, res, next) {
  if(req.params.id==""){
    res.status(400).json({"message":"Url params are required"})
  }
  User.destroy({where:{id:req.params.id }}).then( (result) => res.json(result) ).catch((err)=>{
    res.status(500).json({"message":err.stack})
})
});
/**authenticate  user */
router.post('/auth', function(req, res, next) {
  
  if( !req.body.email || !req.body.password){
    return res.json({auth: false,message:'All Field are Required.',token:null});
  }
  if( req.body.email=="" || req.body.password== "undefined"){
    return res.json({auth: false,message:'Wrong Email.',token:null});
  }
  if( req.body.password=="" || req.body.password== "undefined"){
    return res.json({auth: false,message:'Wrong Password.',token:null});
  }
  User.findOne({where:{ email: req.body.email }}).then( user=> {
    if (!user) return res.json({auth: false,message:'No user found.',token:null});
    var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
    if (!passwordIsValid) return res.json({auth: false,message:'Wrong password.',token:null});
    var token = jwt.sign({ id: user.email }, utils.secret, {
      expiresIn: 86400 // expires in 24 hours
    });
    res.status(200).send({ auth: true, token: token,message:'Success' });
  }).catch((error)=>{
    if (error) return res.json({auth: false,message:error.message,token:null});
  });

});

module.exports = router;
