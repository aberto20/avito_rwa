var multer  =   require('multer');
var express = require('express') ;


var randomString = require('../routes/randomKey') ;
var db =require('../models/index') 
var VerifyToken=require('../utils/VerifyToken')
var router = express.Router();

const product=db.sequelize.models.products;
const audit_logs = db.sequelize.models.audit_logs;

var storage =   multer.diskStorage({
    destination: function (req, file, callback) {
      callback(null, 'public/images');
    },
    filename: function (req, file, callback) {
      callback(null, file.originalname + '-' + Date.now());
    }
  });

  var upload = multer({ storage : storage});

router.get('/',VerifyToken,(req,res,next)=>{
    product.findAll().then( (result) => res.json(result))
  
})

router.post('/',VerifyToken,upload.any(),(req,res,next)=>{
    console.log(req.files)
    
    if(req.body.brand==""){
        res.status(400).json({"message":"brand is required"})
      }
     if(req.body.price==""){
       res.status(400).json({"message":"Price is required"})
     }
     product.create({
        product_id:randomString,
        brand:req.body.brand,
        size:req.body.size,
        price:req.body.price,
        other_details:req.body.other_details
     }).then((data)=>{
         audit_logs.create({
            activity:"create product",
            doneBy:req.doneBy,
            status:"complete",
            comment:`the new product ${req.body.brand} is success created`
         })
         res.json(data)
        }).catch((err)=>{
            audit_logs.create({
                activity:"create product",
                doneBy:req.doneBy,
                status:"fail",
                comment:err.message
             })
         res.status(500).json({"message":err.message})
     })
})
router.put('/:id',VerifyToken,(req,res,next)=>{
    if(req.params.id==""){
        
        res.status(400).json({"message":"Url params are required"})
      }
     product.update({
        brand:req.body.brand,
        size:req.body.size,
        price:req.body.price,
        other_details:req.body.other_details
     },{where:{product_id:req.params.id}}).then((data)=>{
         if(data[0]==1){
            audit_logs.create({
                activity:"update product",
                doneBy:req.doneBy,
                status:"complete",
                comment:`the update product ${req.body.brand} is success created`
             })
         }else{
            audit_logs.create({
                activity:"update product",
                doneBy:req.doneBy,
                status:"fail",
                comment:`Product id ${req.params.id}`
             })
         }
         res.json(data)
        }).catch((err)=>{
            audit_logs.create({
                activity:"update product",
                doneBy:req.doneBy,
                status:"fail",
                comment:err.message
             })
         res.status(500).json({"message":err.message})
     })
})

router.delete('/:id',VerifyToken,(req,res,next)=>{
    if(req.params.id==""){
        res.status(400).json({"message":"Url params are required"})
      }
    
     product.destroy({where:{product_id:req.params.id}}).then((data)=>{
         if(data ==1){
            audit_logs.create({
                activity:"delete product",
                doneBy:req.doneBy,
                status:"complete",
                comment:""
             })
         }else{
            audit_logs.create({
                activity:"delete product",
                doneBy:req.doneBy,
                status:"fail",
                comment:`Product id ${req.params.id}`
             })
         }
         res.json(data)
        }).catch((err)=>{
            audit_logs.create({
                activity:"delete product",
                doneBy:req.doneBy,
                status:"fail",
                comment:err.message
             })
         res.status(500).json({"message":err.message})
     })
})


module.exports = router;
