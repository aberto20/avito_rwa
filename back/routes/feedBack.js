
var express = require('express') ;

var db =require('../models/index') 
var VerifyToken=require('../utils/VerifyToken')
var router = express.Router();

const feedback=db.sequelize.models.feed_backs;
const audit_logs = db.sequelize.models.audit_logs;

router.get('/',VerifyToken,(req,res,next)=>{
    feedback.findAll().then( (result) => res.json(result))
  
})
router.post('/',VerifyToken,(req,res,next)=>{
    if(req.body.comment==""){
        res.status(400).json({"message":"comment is required"})
      }
      feedback.create({
        comment:req.body.comment,
        user_id:req.userId
     }).then((data)=>{
        audit_logs.create({
            activity:"create feedback",
            doneBy:req.doneBy,
            status:"complete",
            comment:`the new feedback is success created`
         })
        res.json(data)}).catch((err)=>{
            audit_logs.create({
                activity:"create feedback",
                doneBy:req.doneBy,
                status:"fail",
                comment:err.message
             })
         res.status(500).json({"message":err.message})
     })
})
router.delete('/:id',VerifyToken,(req,res,next)=>{
    if(req.params.id==""){
        res.status(400).json({"message":"Url params are required"})
      }
   
      feedback.destroy({where:{id:req.params.id}}).then((data)=>{
        audit_logs.create({
            activity:"delete feedback",
            doneBy:req.doneBy,
            status:"complete",
            comment:`the  feedback is success delete`
         })
          res.json(data)
        }).catch((err)=>{
            audit_logs.create({
                activity:"delete feedback",
                doneBy:req.doneBy,
                status:"fail",
                comment:`the feedback is fial delete`
             })
         res.status(500).json({"message":err.message})
     })
})


module.exports = router;
