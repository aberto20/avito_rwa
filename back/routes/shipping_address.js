var express = require('express');
const bcrypt = require('bcrypt');
var router = express.Router();
// var db=require('../models/index');
var db = require('../models/index')
var VerifyToken = require('../utils/VerifyToken')

const shipping_address = db.sequelize.models.shipping_addresses;
const audit_logs = db.sequelize.models.audit_logs;

/**get all  shipping address */
router.get('/', function (req, res, next) {
  shipping_address.findAll().then((result) => res.json(result)).catch((err) => {
    res.status(500).json({ "message": err.message })
  })
});
/**Create new shipping address */
router.post('/', VerifyToken, function (req, res, next) {

  if (req.body.phone == "") {
    res.status(400).json({ "message": "Name are required" })
  }
  if (req.body.address == "") {
    res.status(400).json({ "message": "Email are required" })
  }
  if (req.body.street == "") {
    res.status(400).json({ "message": "Password are required" })
  }

  shipping_address.findOrCreate({
    where: { user_id: req.userId }, defaults: {
      phone: req.body.phone,
      address: req.body.address,
      street: req.body.street,
      user_id: req.userId
    }
  }).then((result) => {
    if (result[1]) {
      audit_logs.create({
        activity:"create shipping address",
        doneBy:req.doneBy,
        status:"complete",
        comment:`the new shipping address is success created`
     })
      res.json(result[0])
    } else {
      audit_logs.create({
        activity:"create shipping address",
        doneBy:req.doneBy,
        status:"fails",
        comment:`the new shipping address is success fail`
     })
      res.status(400).json({ "message": `User ${req.body.names} Exist` })
    }
  }).catch((err) => {
    audit_logs.create({
      activity:"create shipping address",
      doneBy:req.doneBy,
      status:"fails",
      comment:err.message
   })
    res.status(500).json({ "message": err.message })
  })

});
/**update  shipping address */
router.put('/:id', VerifyToken, function (req, res, next) {
  if (req.params.id == "") {
    res.status(400).json({ "message": "Url params are required" })
  }

  shipping_address.update({
    phone: req.body.phone,
    address: req.body.address,
    street: req.body.street,
  }, { where: { id: req.params.id } }).then((result) =>{
    if(result[0]==1){
      audit_logs.create({
        activity:"update shipping address",
        doneBy:req.doneBy,
        status:"complete",
        comment:"update shipping address"
     })
    }else{
      audit_logs.create({
        activity:"update shipping address",
        doneBy:req.doneBy,
        status:"fail",
        comment:"update shipping address"
     })
    }
    res.json(result)
  } ).catch((err) => {
    audit_logs.create({
      activity:"update shipping address",
      doneBy:req.doneBy,
      status:"fail",
      comment:err.message
   })
    res.status(500).json({ "message": err.message })
  })

});

/**delete  shipping address */
router.delete('/:id', VerifyToken, function (req, res, next) {
  if (req.params.id == "") {
    res.status(400).json({ "message": "Url params are required" })
  }
  shipping_address.destroy({ where: { id: req.params.id } }).then((result) =>{
    if(result==1){
      audit_logs.create({
        activity:"delete shipping address",
        doneBy:req.doneBy,
        status:"complete",
        comment:""
     })
    }else{
      audit_logs.create({
        activity:"delete shipping address",
        doneBy:req.doneBy,
        status:"fail",
        comment:""
     })
    }
    res.json(result)
  } ).catch((err) => {
    audit_logs.create({
      activity:"delete shipping address",
      doneBy:req.doneBy,
      status:"complete",
      comment:err.message 
   })
    res.status(500).json({ "message": err.message })
  })
});

module.exports = router;
