var express = require('express');
var router = express.Router();
// var db=require('../models/index');
var db =require('../models/index') 
var VerifyToken=require('../utils/VerifyToken')

const service_offers = db.sequelize.models.service_offers;
const audit_logs = db.sequelize.models.audit_logs;

/**get all  service offer */
router.get('/',VerifyToken, function(req, res, next) {
    service_offers.findAll().then( (result) => res.json(result)).catch((err)=>{
    res.status(500).json({"message":err.message})
})
});
/**Create new service offer */
router.post('/',VerifyToken, function(req, res, next) {

     if(req.body.service_name==""){
       res.status(400).json({"message":"service name are required"})
     }
     service_offers.findOrCreate({where:{service_name:req.body.service_name},defaults:{
        service_name: req.body.service_name,
        is_active: "0"
    }}).then( (result) =>  {
      if(result[1]){
        audit_logs.create({
          activity:"create service offer",
          doneBy:req.doneBy,
          status:"complete",
          comment:`the new service offer ${req.body.service_name} is success created`
       })
        res.json(result[0])
      }else{
        audit_logs.create({
          activity:"create service offer",
          doneBy:req.doneBy,
          status:"fail",
          comment:`the new service offer ${req.body.service_name} is Exist`
       })
        res.status(400).json({"message":`service offers ${req.body.names} Exist`})
      }
    } ).catch((err)=>{
      audit_logs.create({
        activity:"create service offer",
        doneBy:req.doneBy,
        status:"fail",
        comment:err.message
     })
      res.status(500).json({"message":err.message})
  })

});
/**update  service offer */
router.put('/:id',VerifyToken, function(req, res, next) {
  if(req.params.id==""){
    res.status(400).json({"message":"Url params are required"})
  }
  
  service_offers.update({
    service_name: req.body.service_name,
    is_active: req.body.is_active
 },{where:{id:req.params.id }}).then( (result) =>{
    if(result[0]==1){
      audit_logs.create({
        activity:"update service offer",
        doneBy:req.doneBy,
        status:"complete",
        comment:`the  service offer ${req.body.service_name} is success update`
     })
    }else{
      audit_logs.create({
        activity:"update service offer",
        doneBy:req.doneBy,
        status:"fail",
        comment:`the  service offer ${req.body.service_name} fails update`
     })
    }
 res.json(result)
 } ).catch((err)=>{
  audit_logs.create({
    activity:"update service offer",
    doneBy:req.doneBy,
    status:"fail",
    comment:err.message
 })
  res.status(500).json({"message":err.message})
})

});

/**delete  user */
router.delete('/:id',VerifyToken, function(req, res, next) {
  if(req.params.id==""){
    res.status(400).json({"message":"Url params are required"})
  }
  service_offers.destroy({where:{id:req.params.id }}).then( (result) =>{
    if(result==1){
      audit_logs.create({
        activity:"delete service offer",
        doneBy:req.doneBy,
        status:"complete",
        comment:"delete service offer is success"
     })
    }else{
      audit_logs.create({
        activity:"delete service offer",
        doneBy:req.doneBy,
        status:"fail",
        comment:"delete service offer is success"
     })
    }
   res.json(result)
  }  ).catch((err)=>{
    audit_logs.create({
      activity:"delete service offer",
      doneBy:req.doneBy,
      status:"fail",
      comment:err.message
   })
    res.status(500).json({"message":err.message})
})
});

module.exports = router;
