'use strict';
module.exports = (sequelize, DataTypes) => {
  const user_orders_products = sequelize.define('user_orders_products', {
    order_id: DataTypes.STRING,
    product_id: DataTypes.STRING,
    qty: DataTypes.INTEGER,
    comment: DataTypes.TEXT,
    user_id:DataTypes.INTEGER,
  }, {});
  user_orders_products.associate = function(models) {
    // associations can be defined here
  };
  return user_orders_products;
};