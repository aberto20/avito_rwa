'use strict';
module.exports = (sequelize, DataTypes) => {
  const feed_backs = sequelize.define('feed_backs', {
    comment: DataTypes.TEXT,
    user_id: DataTypes.INTEGER
  }, {});
  feed_backs.associate = function(models) {
    // associations can be defined here
  };
  return feed_backs;
};