'use strict';
module.exports = (sequelize, DataTypes) => {
  const products = sequelize.define('products', {
    product_id: DataTypes.STRING,
    brand: DataTypes.STRING,
    size: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    other_details: DataTypes.TEXT
  }, {});
  products.associate = function(models) {
    // associations can be defined here
  };
  return products;
};