'use strict';
module.exports = (sequelize, DataTypes) => {
  const service_offers = sequelize.define('service_offers', {
    service_name: DataTypes.STRING,
    is_active: DataTypes.STRING
  }, {});
  service_offers.associate = function(models) {
    // associations can be defined here
  };
  return service_offers;
};