'use strict';
module.exports = (sequelize, DataTypes) => {
  const user_order_deliveries = sequelize.define('user_order_deliveries', {
    order_id: DataTypes.INTEGER,
    delivery_status: DataTypes.STRING,
  }, {});
  user_order_deliveries.associate = function(models) {
    // associations can be defined here
  };
  return user_order_deliveries;
};