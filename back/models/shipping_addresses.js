'use strict';
module.exports = (sequelize, DataTypes) => {
  const shipping_addresses = sequelize.define('shipping_addresses', {
    phone: DataTypes.STRING,
    address: DataTypes.STRING,
    street: DataTypes.STRING,
    user_id:DataTypes.INTEGER
  }, {});
  shipping_addresses.associate = function(models) {
    // associations can be defined here
  };
  return shipping_addresses;
};