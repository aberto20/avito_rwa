'use strict';
module.exports = (sequelize, DataTypes) => {
  const audit_logs = sequelize.define('audit_logs', {
    activity: DataTypes.STRING,
    doneBy: DataTypes.STRING,
    status: DataTypes.STRING,
    comment: DataTypes.TEXT
  }, {});
  audit_logs.associate = function(models) {
    // associations can be defined here
  };
  return audit_logs;
};