var jwt = require('jsonwebtoken');
var utils = require("./secret");
var db =require('../models/index') 
const User = db.sequelize.models.user;

function verifyToken(req, res, next) {
  var token = req.headers['x-access-token'];
  if (!token)
    return res.json({ auth: false, message: 'No token provided.' });
  jwt.verify(token, utils.secret, function(err, decoded) {
    if (err)
    return res.json({ auth: false, message: 'Failed to authenticate token.' });
    // if everything good, save to request for use in other routes
    User.findOne({where:{ email: decoded.id }}).then( user=> {
      if (!user) return res.json({auth: false,message:'No user found.',token:null});
      req.userId = user.id;
      req.doneBy = user.names;
      console.log(req.userId+"-"+req.doneBy)
      next();
    }).catch((error)=>{
      if (error) return res.json({auth: false,message:error.message,token:null});
    });
  
  });
}
module.exports = verifyToken;